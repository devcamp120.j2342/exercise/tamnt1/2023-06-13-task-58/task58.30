package com.devcamp.customerrestapi.Respository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.customerrestapi.models.Customer;

public interface CustomerRespository extends CrudRepository<Customer, Integer> {

}
