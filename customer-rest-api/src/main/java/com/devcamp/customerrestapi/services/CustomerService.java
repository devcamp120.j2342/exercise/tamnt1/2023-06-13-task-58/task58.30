package com.devcamp.customerrestapi.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.customerrestapi.Respository.CustomerRespository;
import com.devcamp.customerrestapi.models.Customer;

@Service
public class CustomerService {
    public final CustomerRespository customerRespository;

    public CustomerService(CustomerRespository customerRespository) {
        this.customerRespository = customerRespository;
    }

    public void createCustomer() {
        List<Customer> customers = new ArrayList<>();
        Date currentDate = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Customer customer1 = new Customer("John Doe", "john@example.com", "123456789", "123 Main St",
                dateFormat.format(currentDate), dateFormat.format(currentDate));
        customers.add(customer1);

        Customer customer2 = new Customer("Tam Ng", "tam@example.com", "987654321", "456 Elm St",
                dateFormat.format(currentDate),
                dateFormat.format(currentDate));
        customers.add(customer2);

        Customer customer3 = new Customer("Minh Johnson", "minh@example.com", "555555555", "789 Oak St",
                dateFormat.format(currentDate),
                dateFormat.format(currentDate));
        customers.add(customer3);
        customerRespository.saveAll(customers);

    }

    public List<Customer> getCustomerList() {
        List<Customer> customerList = (List<Customer>) customerRespository.findAll();
        return customerList;
    }
}
