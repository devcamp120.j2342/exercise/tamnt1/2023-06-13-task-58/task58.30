/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

const gCUSTOMER_URL = "http://localhost:8080/customers";
const gCOLUMN_ID = {
    stt: 0,
    hoTen: 1,
    email: 2,
    soDienThoai: 3,
    diaChi:4,
    ngayTao: 5,
    ngayCapNhat:6,

}
const gCOL_NAME = [
    "stt",
    "hoTen",
    "email",
    "soDienThoai",
    "diaChi",
    "ngayTao",
    "ngayCapNhat",

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()

        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getVoucherList() {
        this.vApi.onGetVoucherClick((paramCustomer) => {
            console.log(paramCustomer)
            this._createOrderTable(paramCustomer)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createOrderTable(paramCustomer) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-order')) {
            $('#table-order').DataTable().destroy();
        }
        const vOrderTable = $("#table-order").DataTable({
            // Khai báo các cột của datatable
            "columns": [
                { "data": gCOL_NAME[gCOLUMN_ID.stt] },
                { "data": gCOL_NAME[gCOLUMN_ID.hoTen] },
                { "data": gCOL_NAME[gCOLUMN_ID.email] },
                { "data": gCOL_NAME[gCOLUMN_ID.soDienThoai] },
                { "data": gCOL_NAME[gCOLUMN_ID.soDienThoai] },
                { "data": gCOL_NAME[gCOLUMN_ID.diaChi] },
                { "data": gCOL_NAME[gCOLUMN_ID.ngayCapNhat] },
        
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
               
                {
                    targets: gCOLUMN_ID.stt,
                    render: function() {
                       return stt++
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramCustomer) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._getVoucherList()
    }
}

/*** REGION 3 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onGetVoucherClick(paramCallbackFn) {
     
        $.ajax({
            url: gCUSTOMER_URL,
            method: "GET",
            success: function (data) {
                paramCallbackFn(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
    }

}